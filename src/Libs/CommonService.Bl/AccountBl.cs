﻿using CommonService.Bl.Interfaces;
using CommonService.Common.Helpers;
using CommonService.Dal.Interfaces.Repositories;
using CommonService.Mapper.BeModels;
using CommonService.Models.Be.Accounts;
using CommonService.Models.Be.Enums;
using CommonService.Models.Be.Subscriptions;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl
{
    public class AccountBl : IAccountBl
    {
        private readonly IAccountRepository _userRepository;
        private readonly IChannelRepository _channelRepository;
        private readonly IEventCategoryRepository _eventCategoryRepository;
        public AccountBl(IAccountRepository userRepository, IChannelRepository channelRepository, IEventCategoryRepository eventCategoryRepository)
        {
            if (userRepository == null) throw new ArgumentNullException("userRepository");
            if (channelRepository == null) throw new ArgumentNullException("channelRepository");
            if (eventCategoryRepository == null) throw new ArgumentNullException("tapeRepository");

            _userRepository = userRepository;
            _channelRepository = channelRepository;
            _eventCategoryRepository = eventCategoryRepository;
        }

        public async Task<IEnumerable<Account>> GetUsersByTapeAsync(EventCategoryTypeEnum tapeEnum)
        {
            var users = await _userRepository.GetAllByEventCategoryAsync(tapeEnum.ToString());
            //var users = GetFakeUser();
            return AccountMapper.Map(users);
        }


        public async Task<Account> Activate(Guid userId)
        {

            var user = await _userRepository.GetAsync(userId);
            if (user.Activated)
            {
                throw new Exception("Пользователь уже активирован");
            }

            user.Activated = true;

            var updatedUser = await _userRepository.UpdateAsync(user);
            return AccountMapper.Map(updatedUser);
        }

        public async Task<Account> Diactivate(Guid userId)
        {
            var user = await _userRepository.GetAsync(userId);
            if (!user.Activated)
            {
                throw new Exception("Пользователь уже деактивирован");
            }

            user.Activated = false;

            var updatedUser = await _userRepository.UpdateAsync(user);
            return AccountMapper.Map(updatedUser);
        }

        public async Task<Account> CreateAsync(AccountCreate model)
        {
            await CheckLogin(model.Login);

            bool needAllEvents = false;
            var replaceSubs = new List<SubscriptionCreate>();
            
            await CheckCreateData(model);

            var userId = Guid.NewGuid();

            if (model.Subscriptions != null && model.Subscriptions.Any(x => x.EventCategoryType == EventCategoryTypeEnum.All))
            {
                needAllEvents = true;

                var eventTypes = EnumHelper<EventCategoryTypeEnum>.GetCollectionWithoutCurrent(EventCategoryTypeEnum.All);
                
                foreach (var contact in model.Contacts)
                {
                    foreach (var eventType in eventTypes)
                    {
                        replaceSubs.Add(new SubscriptionCreate()
                        {
                            AccountId = userId,
                            ChannelType = (ChannelTypeEnum) contact.Type,
                            EventCategoryType = eventType,
                        });
                    }
                }
            }

            var user = new AccountDb()
            {
                Id = userId,
                CreateDate = DateTime.Now,
                Login = model.Login,
                Password = model.Password,
                Activated = false,
                Contacts = model.Contacts == null ? null : model.Contacts.Select(x => new AccountContactDb()
                {
                    CreateDate = DateTime.Now,
                    Id = Guid.NewGuid(),
                    Type = x.Type.ToString(),
                    Value = x.Value,
                }).ToList(),
                Subscriptions = needAllEvents ? BuildSubs(replaceSubs, userId) : model.Subscriptions == null ? null : BuildSubs(model.Subscriptions, userId)
            };

            var createdUser = await _userRepository.CreateAsync(user);
            return AccountMapper.Map(createdUser);
        }    
        
        public async Task CheckLogin(string login)
        {
            var account = await _userRepository.GetAllAsync();
            if(account.Any(x=>x.Login.ToLower() == login))
            {
                throw new Exception("Данный логин использовать невозможно");
            }
        }

        private IEnumerable<SubscriptionDb> BuildSubs(IEnumerable<SubscriptionCreate> subscriptions, Guid userId)
        {
            var subs = subscriptions.Select(x => new SubscriptionDb()
            {
                Id = Guid.NewGuid(),
                CreateDate = DateTime.Now,
                Channel = x.ChannelType.ToString(),
                EventCategory = x.EventCategoryType.ToString(),
                AccountId = userId,
            }).ToList();

            return subs;
        }

        private async Task CheckCreateData(AccountCreate model)
        {
            if (string.IsNullOrWhiteSpace(model.Password))
            {
                throw new Exception("Пароль не может быть пустым или состоять из пробелов");
            }

            var users = await _userRepository.GetByLogin(model.Login);
            if (users.Count() > 0)
            {
                throw new Exception("Логин используется");
            }

            foreach (var contact in model.Contacts)
            {
                var userContacts = await _userRepository.GetByContact(contact.Type.ToString(), contact.Value);
                if (userContacts.Count() > 0)
                {
                    throw new Exception("Указанные контакт/ы уже созданы");
                }
            }
        }

        public Task<Account> ChangeSubscriptions(Guid userId, IEnumerable<Subscription> channels)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Account>> GetAll()
        {
            var users = await _userRepository.GetAllAsync();
            return AccountMapper.Map(users);           
        }

        public async Task<Account> GetById(Guid id)
        {
            var user = await _userRepository.GetFullAsync(id);
            return AccountMapper.Map(user);
        }


        //public async Task<User> ChangeEventCategoriesAsync(Guid userId, IEnumerable<EventCategory> eventCategories)
        //{
        //    var user = await _userRepository.GetAsync(userId);         

        //    // удаление 
        //    foreach (var eventCategory in user.EventCategories)
        //    {
        //        if(eventCategories.Any(x=>x.Type.ToString() == eventCategory.Type))
        //        {

        //        }
        //        else
        //        {
        //            user.EventCategories.ToList().Remove(_eventCategoryRepository.GetByType(eventCategory.Type));
        //        }
        //    }

        //    // добавление
        //    foreach (var eventCategory in eventCategories)
        //    {
        //        if (user.EventCategories.Any(x => x.Type == eventCategory.Type.ToString()))
        //        {

        //        }
        //        else
        //        {
        //            user.EventCategories.ToList().Add(_eventCategoryRepository.GetByType(eventCategory.Type.ToString()));
        //        }
        //    }


        //    var updatedUser = await _userRepository.UpdateAsync(user);

        //    return UserMapper.Map(updatedUser);
        //}

        //public async Task<User> ChangeChannelsAsync(Guid userId, IEnumerable<Channel> channels)
        //{
        //    var user = await _userRepository.GetAsync(userId);

        //    // удаление 
        //    foreach (var channel in user.Channels)
        //    {
        //        if (channels.Any(x => x.Type.ToString() == channel.Type))
        //        {

        //        }
        //        else
        //        {
        //            user.Channels.ToList().Remove(_channelRepository.GetByType(channel.Type));
        //        }
        //    }

        //    // добавление
        //    foreach (var channel in channels)
        //    {
        //        if (user.Channels.Any(x => x.Type == channel.Type.ToString()))
        //        {

        //        }
        //        else
        //        {
        //            user.Channels.ToList().Add(_channelRepository.GetByType(channel.Type.ToString()));
        //        }
        //    }


        //    var updatedUser = await _userRepository.UpdateAsync(user);

        //    return UserMapper.Map(updatedUser);
        //}


    }
}
