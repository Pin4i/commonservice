﻿using CommonService.Models.Be.Messages;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Bl.Builders
{
    public class MessageBuilder
    {
        public static IDictionary<MessageInfo, MessageDb> BuildDb(Message message, AccountDb account)
        {
            var messages = new Dictionary<MessageInfo, MessageDb>();

            var eventType = message.EventCategory;
            var subscriptions = account.Subscriptions.Where(x => x.EventCategory == eventType.ToString());

            foreach (var subscription in subscriptions)
            {
                var accountContacts = account.Contacts.Where(x => x.Type == subscription.Channel);

                foreach (var sendTo in accountContacts)
                {
                    var messageDb = new MessageDb()
                    {
                        Additional = message.Additional,
                        CreateDate = DateTime.Now,
                        EventCategoryType = message.EventCategory.ToString(),
                        EventDate = message.EventDate,
                        Id = Guid.NewGuid(),
                        Theme = message.Theme,
                        Text = message.Text,
                        Tags = message.Tags.ToString(),
                        Links = message.Links.ToString(),
                        ShippingInformations = new List<ShippingInformationDb>()
                        {
                            new ShippingInformationDb()
                            {
                                Id = Guid.NewGuid(),
                                CreateDate = DateTime.Now,
                                Channel = subscription.Channel.ToString(),
                                UserId = account.Id,
                                EventCategory = message.EventCategory.ToString(),
                                Status = ShippingStatus.InProcessing.ToString(),
                                SendTo = sendTo.Value
                            }
                        }
                    };

                    var messageInfo = new MessageInfo()
                    {
                        Links = message.Links,
                        Tags = message.Tags,
                        SendTo = sendTo.Value,
                        ChanelType = subscription.Channel.ToString(),
                        AccountId = account.Id,
                    };

                    messages.Add(messageInfo, messageDb);
                }
            }
            return messages;
        }
    }
}
