﻿using CommonService.Bl.Interfaces;
using CommonService.Dal.Interfaces.Repositories;
using CommonService.Mapper.BeModels;
using CommonService.Models.Be.Cameras;
using CommonService.Models.Be.Enums;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl
{
    public class CameraBl : ICameraBl
    {
        private readonly ICameraRepository _cameraRepository;
        private readonly IHttpRequest _httpRequest;
        public CameraBl(ICameraRepository cameraRepository, IHttpRequest httpRequest)
        {
            _cameraRepository = cameraRepository;
            _httpRequest = httpRequest;
        }
        public async Task<Camera> CreateAsync(CameraCreate beModel)
        {
            var camera = new CameraDb()
            {
                BaseUrl = beModel.BaseUrl,
                CreateDate = DateTime.Now,
                Id = Guid.NewGuid(),
                Ip = beModel.Ip,
                Login = beModel.Login,
                Name = beModel.Name,
                OperationUrl = beModel.OperationUrl,
                Password = beModel.Password,
                Port = beModel.Port,
            };

            var createdCamera = await _cameraRepository.CreateAsync(camera);
            return CameraMap.Map(createdCamera);
        }

        public async Task<Camera> UpdateAsync(CameraUpdate beModel)
        {
            var camera = await _cameraRepository.GetAsync(beModel.Id);
            if (camera == null)
                throw new Exception("Камера не найдена");

            camera.BaseUrl = beModel.BaseUrl;

            camera.Ip = beModel.Ip;
            camera.Login = beModel.Login;
            camera.Name = beModel.Name;
            camera.OperationUrl = beModel.OperationUrl;
            camera.Password = beModel.Password;
            camera.Port = beModel.Port;

            var updatedCamera = await _cameraRepository.UpdateAsync(camera);
            return CameraMap.Map(updatedCamera);
        }
        public async Task<Camera> GetAsync(Guid id)
        {
            var camera = await _cameraRepository.GetAsync(id);
            if (camera == null)
                throw new Exception("Камера не найдена");

            return CameraMap.Map(camera);
        }

        public async Task<IEnumerable<Camera>> GetAllAsync()
        {
            var cameras = await _cameraRepository.GetAllAsync();
            if (cameras == null)
                throw new Exception("Камера не найдена");

            return CameraMap.Map(cameras);
        }

        public async Task DeleteAsync(Guid id)
        {
            var camera = await _cameraRepository.GetAsync(id);
            if (camera == null)
                throw new Exception("Камера не найдена");

            _cameraRepository.DeleteNow(camera);
        }



        public async Task<Stream> GetPhotoAsync(Guid cameraId)
        {
            var camera = await _cameraRepository.GetAsync(cameraId);
            if (camera == null)
                throw new Exception("Камера не найдена");

            //admin:Dlin04061608!@
            var result = await _httpRequest.Get(camera.OperationUrl, camera.Login + ":" + camera.Password, "", HttpAuthType.Basic);

            return new System.IO.MemoryStream(result);
        }
    }
}
