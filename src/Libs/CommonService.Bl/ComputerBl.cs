﻿using CommonService.Bl.Interfaces;
using CommonService.Common.AppSettings;
using CommonService.Dal.Interfaces.Repositories;
using CommonService.Mapper.BeModels;
using CommonService.Models.Be.Computers;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl
{
    public class ComputerBl : IComputerBl
    {
        private readonly IComputerRepository _computerRepository;
        private readonly INetworkAdapterRepository _networkAdapterRepository;
        private readonly ISettingsProvider _wakeOnLanSettings;
        public ComputerBl(IComputerRepository computerRepository, INetworkAdapterRepository networkAdapterRepository, ISettingsProvider wakeOnLanSettings)
        {
            _computerRepository = computerRepository;
            _networkAdapterRepository = networkAdapterRepository;
            _wakeOnLanSettings = wakeOnLanSettings;
        }

        public async Task<Computer> Create(ComputerCreate model)
        {
            var computerId = Guid.NewGuid();
            var newComputer = new ComputerDb()
            {
                Id = computerId,
                CreateDate = DateTime.Now,
                Description = model.Description,
                Name = model.Name,
                NetworkAdapters = model.Adapters.Select(x => new NetworkAdapterDb()
                {
                    ComputerId = computerId,
                    CreateDate = DateTime.Now,
                    Ip = x.Ip,
                    Id = Guid.NewGuid(),
                    Mac = x.Mac,
                    Name = x.Name,
                    Port = x.Port,
                })
            };

            var createdComp = await _computerRepository.CreateAsync(newComputer);
            return ComputerMapper.Map(createdComp);
        }

        public async Task<Computer> Update(ComputerUpdate model)
        {
            var computer = await _computerRepository.GetAsync(model.Id);
            computer.Name = model.Name;
            computer.Description = model.Description;

            var updatedComputer = await _computerRepository.UpdateAsync(computer);
            return ComputerMapper.Map(updatedComputer);
        }


        public async Task<IEnumerable<Computer>> GetAll()
        {
            var computers = await _computerRepository.GetAllAsync();
            return ComputerMapper.Map(computers);
        }

        public async Task<Computer> Get(Guid id)
        {
            var x = await _computerRepository.GetAsync(id);
            return ComputerMapper.Map(x);
        }

        public async Task Delete(Guid id)
        {
            throw new NotImplementedException();
        }


        #region POWER ON
        public void PowerOnById(Guid id)
        {
            var computer = _computerRepository.GetAsync(id).Result;

            foreach (var adapter in computer.NetworkAdapters)
            {
                WakeUp(adapter.Mac);
            }
        }

        public void PowerOnByIp(string ip)
        {
            var adapters = _networkAdapterRepository.GetByIp(ip).Result;

            foreach (var adapter in adapters)
            {
                WakeUp(adapter.Mac);
            }
        }

        public void PowerOnByMac(string mac)
        {
            var adapter = _networkAdapterRepository.GetByMac(mac).Result;
            WakeUp(adapter.Mac);
        }

        public void PowerOnByName(string name)
        {
            var computers = _computerRepository.GetByName(name).Result;

            foreach (var comp in computers)
            {
                foreach (var adapter in comp.NetworkAdapters)
                {
                    WakeUp(adapter.Mac);
                }
            }
        }

        private void WakeUp(string adapterMac)
        {
            string mac = adapterMac;
            byte[] arr = mac.Split(':').Select(x => Convert.ToByte(x, 16)).ToArray();
            var pc = new PhysicalAddress(arr);
            pc.SendWol();
        }

        private void OtherSend()
        {
            //string mac = "b8:27:eb:97:b6:39";
            //byte[] arr = mac.Split(':').Select(x => Convert.ToByte(x, 16)).ToArray();

            //// Using the IPAddess extension
            //IPAddress.Broadcast.SendWol(0x00, 0x11, 0x22, 0x33, 0x44, 0x55);

            //// via core MagicPacket class
            //var endPoint = new IPEndPoint(IPAddress.Broadcast, 7); // You don't have to use Broadcast.
            //                                                       // Every IP/port-combination is possible.
            //MagicPacket.Send(endPoint, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55);

            //// via IPEndPoint extension
            //endPoint.SendWol(0x00, 0x11, 0x22, 0x33, 0x44, 0x55);

            //// ...
            //using System.Net.NetworkInformation;
            //PhysicalAddress.Parse("00-11-22-33-44-55").SendWol();
        }
        #endregion

    }
}
