﻿using CommonService.Bl.Interfaces;
using CommonService.Common.AppSettings;
using CommonService.Models.Be.Enums;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl.Helper
{
    public class HttpRequest : IHttpRequest
    {
        private readonly ISettingsProvider _settingsProvider;
        public HttpRequest(ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
        }

        public async Task Get()
        {
        }

        public async Task<string> PostAsync(string url, string json)
        {
            var client = GetClient(HttpAuthType.NoAuth);
            client.BaseAddress = new Uri(_settingsProvider.MessageServiceUrl);
            var content = new StringContent(json);
            var response = await client.PostAsync(url, content);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception();
            }

            var responceReuslt = await response.Content.ReadAsStringAsync();
            return responceReuslt;
        }


        public async Task<byte[]> Get(string url, string user, string password, HttpAuthType type)
        {
            var httpClient = GetClient(type, user, password);
            var response = await httpClient.GetAsync(url);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception();
            }

            var myBytes = await response.Content.ReadAsByteArrayAsync();
            string convertedFromString = Convert.ToBase64String(myBytes);


            return myBytes;
        }

        public async Task Post()
        {

        }

        private HttpClient GetClientBasicAuth(string userAndPass)
        {
            var httpClient = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes(userAndPass);
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            return httpClient;
        }

        private HttpClient GetClient(HttpAuthType type, params string[] authParams)
        {
            if (type == HttpAuthType.Basic)
            {
                return GetClientBasicAuth(authParams[0]);
            }
            else
            {
                var httpClient = new HttpClient();
                return httpClient;
            }
        }
    }
}
