﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Bl.Helper
{
    public class MyStringComparer : IEqualityComparer<string>
    {
        public int GetHashCode(string co)
        {
            if (co == null)
            {
                return 0;
            }
            return co.GetHashCode();
        }

        public bool Equals(string x1, string x2)
        {
            if (object.ReferenceEquals(x1, x2))
            {
                return true;
            }
            if (object.ReferenceEquals(x1, null) ||
                object.ReferenceEquals(x2, null))
            {
                return false;
            }
            return x1 == x2;
        }
    }
}
