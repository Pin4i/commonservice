﻿using CommonService.Models.Be.Accounts;
using CommonService.Models.Be.Enums;
using CommonService.Models.Be.Subscriptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl.Interfaces
{
    public interface IAccountBl
    {
        Task<IEnumerable<Account>> GetUsersByTapeAsync(EventCategoryTypeEnum tapeEnum);
        Task<Account> Diactivate(Guid userId);
        Task<Account> Activate(Guid userId);
        Task<Account> CreateAsync(AccountCreate model);
        //Task<User> ChangeEventCategoriesAsync(Guid userId, IEnumerable<EventCategory> tapes);
        //Task<User> ChangeChannelsAsync(Guid userId, IEnumerable<Channel> channels);
        Task<Account> ChangeSubscriptions(Guid userId, IEnumerable<Subscription> channels);
        Task<IEnumerable<Account>> GetAll();
        Task<Account> GetById(Guid id);
    }
}
