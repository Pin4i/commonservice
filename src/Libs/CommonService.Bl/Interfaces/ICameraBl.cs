﻿using CommonService.Models.Be.Cameras;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl.Interfaces
{
    public interface ICameraBl
    {
        Task<Camera> CreateAsync(CameraCreate beModel);
        Task<Camera> UpdateAsync(CameraUpdate beModel);
        Task<Camera> GetAsync(Guid id);
        Task<IEnumerable<Camera>> GetAllAsync();
        Task DeleteAsync(Guid id);
        Task<Stream> GetPhotoAsync(Guid cameraId);
    }
}
