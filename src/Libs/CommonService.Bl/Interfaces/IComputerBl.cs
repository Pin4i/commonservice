﻿using CommonService.Models.Be.Computers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl.Interfaces
{
    public interface IComputerBl
    {
        Task<Computer> Create(ComputerCreate model);
        Task<Computer> Update(ComputerUpdate model);
        Task<IEnumerable<Computer>> GetAll();
        Task<Computer> Get(Guid id);
        Task Delete(Guid id);






        void PowerOnByName(string name);
        void PowerOnByMac(string mac);
        void PowerOnByIp(string ip);
        void PowerOnById(Guid id);
    }
}
