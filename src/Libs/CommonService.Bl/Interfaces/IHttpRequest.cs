﻿using CommonService.Models.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl.Interfaces
{
    public interface IHttpRequest
    {
        Task Get();

        Task Post();

        Task<byte[]> Get(string url, string user, string password, HttpAuthType type);
        Task<string> PostAsync(string url, string json);
    }
}
