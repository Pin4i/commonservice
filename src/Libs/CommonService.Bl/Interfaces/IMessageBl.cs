﻿using CommonService.Models.Be.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl.Interfaces
{
    public interface IMessageBl
    {
        Task SendMessage(Message message);
        Task SendMessage(IEnumerable<Message> messages);
    }
}
