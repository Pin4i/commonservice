﻿using CommonService.Models.Be.NetworkAdapters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl.Interfaces
{
    public interface INetworkAdapterBl
    {
        Task Create(NetworkAdapterCreate model);

        Task Update(NetworkAdapterUpdate model);

        Task<NetworkAdapter> Get(Guid id);

        Task<IEnumerable<NetworkAdapter>> GetAll();

        Task Delete(Guid id);
    }
}
