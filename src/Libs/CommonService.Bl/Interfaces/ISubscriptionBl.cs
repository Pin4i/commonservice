﻿using CommonService.Models.Be.Accounts;
using CommonService.Models.Be.Subscriptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl.Interfaces
{
    public interface ISubscriptionBl
    {
        Task Create(Guid userId, SubscriptionCreate model);
        Task Delete(Guid id);
        Task<Account> GetUserByContact(AccountByContact userByContact);
    }
}
