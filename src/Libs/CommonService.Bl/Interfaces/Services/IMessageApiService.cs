﻿using CommonService.Models.Be;
using CommonService.Models.Be.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl.Interfaces.Services
{
    public interface IMessageApiService
    {
        Task<ResponceResult> SendMessage(CommonMessage commonMessage);
    }
}
