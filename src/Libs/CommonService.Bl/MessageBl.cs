﻿using CommonService.Bl.Builders;
using CommonService.Bl.Interfaces;
using CommonService.Bl.Interfaces.Services;
using CommonService.Dal.Interfaces.Repositories;
using CommonService.Models.Be.Enums;
using CommonService.Models.Be.Messages;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl
{
    public class MessageBl : IMessageBl
    {
        private readonly ISenderService _serviceSender;
        private readonly IMessageRepository _messageRepository;
        private readonly IAccountRepository _accountRepository;

        public MessageBl(ISenderService serviceSender, IMessageRepository messageRepository, IAccountRepository accountRepository)
        {
            if (serviceSender == null) throw new ArgumentNullException("serviceSender");
            if (messageRepository == null) throw new ArgumentNullException("messageRepository");
            if (accountRepository == null) throw new ArgumentNullException("accountRepository");

            _serviceSender = serviceSender;
            _messageRepository = messageRepository;
            _accountRepository = accountRepository;
        }

        public async Task SendMessage(IEnumerable<Message> messages)
        {
            foreach (var msg in messages)
            {
                await SendMessage(msg);
            }
        }

        public async Task SendMessage(Message message)
        {
            var accounts = await _accountRepository.GetAllByEventCategoryAsync(message.EventCategory.ToString().ToLower());
            var createdMessages = new Dictionary<MessageInfo, MessageDb>();

            foreach (var account in accounts)
            {
                var buildedMessages = MessageBuilder.BuildDb(message, account);
                foreach(var buildedMessage in buildedMessages)
                {
                    createdMessages.Add(buildedMessage.Key, await _messageRepository.CreateAsync(buildedMessage.Value));
                }
            }

            foreach (var messageToDelivery in createdMessages)
            {
                
                var result = await _serviceSender.SendText(new CommonMessage()
                {
                    Additional = messageToDelivery.Value.Additional,
                    Links = messageToDelivery.Key.Links,
                    Tags = messageToDelivery.Key.Tags,
                    Text = messageToDelivery.Value.Text,
                    Theme = messageToDelivery.Value.Theme,
                    To = messageToDelivery.Key.SendTo,
                    ChannelType = Enum.TryParse<ChannelTypeEnum>(messageToDelivery.Key.ChanelType, out var res) ? res : throw new Exception()
                });

                var newShippingInfo = new ShippingInformationDb()
                {
                    Id = Guid.NewGuid(),
                    CreateDate = DateTime.Now,
                    Channel = messageToDelivery.Key.ChanelType,
                    UserId = messageToDelivery.Key.AccountId,
                    EventCategory = messageToDelivery.Value.EventCategoryType,
                    Status = result.Sended ? ShippingStatus.Error.ToString() : ShippingStatus.Sended.ToString(),
                    SendTo = messageToDelivery.Key.SendTo,
                    Error = result.Sended ? result.Error : null,
                };

                messageToDelivery.Value.ShippingInformations.ToList().Add(newShippingInfo);
                await _messageRepository.UpdateAsync(messageToDelivery.Value);
            }            
        }
    }
}
