﻿using CommonService.Bl.Interfaces;
using CommonService.Dal.Interfaces.Repositories;
using CommonService.Models.Be.NetworkAdapters;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl
{
    public class NetworkAdapterBl : INetworkAdapterBl
    {
        private readonly IComputerRepository _computerRepository;
        private readonly INetworkAdapterRepository _networkAdapterRepository;
        public NetworkAdapterBl(IComputerRepository computerRepository, INetworkAdapterRepository networkAdapterRepository)
        {
            _computerRepository = computerRepository;
            _networkAdapterRepository = networkAdapterRepository;
        }

        public async Task Create(NetworkAdapterCreate model)
        {
            var computer = await _computerRepository.GetAsync(model.ComputerId);

            var newAdapter = new NetworkAdapterDb()
            {
                ComputerId = computer.Id,
                CreateDate = DateTime.Now,
                Id = Guid.NewGuid(),
                Ip = model.Ip,
                Name = model.Name,
                Mac = model.Mac,
                Port = model.Port,
            };

            var createdAdapter = await _networkAdapterRepository.CreateAsync(newAdapter);
        }

        public async Task Update(NetworkAdapterUpdate model)
        {
            var adapter = await _networkAdapterRepository.GetAsync(model.Id);
            adapter.Port = model.Port;
            adapter.Name = model.Name;
            adapter.Mac = model.Mac;
            adapter.Ip = model.Ip;
            adapter.ComputerId = model.ComputerId;

            var updatedAdapter = await _networkAdapterRepository.UpdateAsync(adapter);
        }

        public async Task<NetworkAdapter> Get(Guid id)
        {
            var adapter = await _networkAdapterRepository.GetAsync(id);
            if (adapter == null)
                throw new Exception();

            return new NetworkAdapter()
            {
                Id = adapter.Id,
                Ip = adapter.Ip,
                Mac = adapter.Mac,
                Name = adapter.Name,
                Port = adapter.Port
            };
        }

        public async Task<IEnumerable<NetworkAdapter>> GetAll()
        {
            var adapters = await _networkAdapterRepository.GetAllAsync();

            return adapters.Select(adapter => new NetworkAdapter()
            {
                Id = adapter.Id,
                Ip = adapter.Ip,
                Mac = adapter.Mac,
                Name = adapter.Name,
                Port = adapter.Port
            });
        }

        public async Task Delete(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
