﻿using CommonService.Bl.Interfaces;
using CommonService.Bl.Interfaces.Services;
using CommonService.Models.Be;
using CommonService.Models.Be.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl.Services
{
    public class MessageApiService : IMessageApiService
    {
        private readonly IHttpRequest _httpRequest;
        public MessageApiService(IHttpRequest httpRequest)
        {
            _httpRequest = httpRequest;
        }

        public async Task<ResponceResult> SendMessage(CommonMessage commonMessage)
        {
            var url = "";
            var jsonMessage = JsonConvert.SerializeObject(commonMessage);
            
            var responce = await _httpRequest.PostAsync(url, jsonMessage);
            return JsonConvert.DeserializeObject<ResponceResult>(responce);
        }
    }
}
