﻿using CommonService.Bl.Interfaces.Services;
using CommonService.Models.Be;
using CommonService.Models.Be.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl.Services
{
    public class SenderService : ISenderService
    {
        private readonly IMessageApiService _messageApiService;
        public SenderService(IMessageApiService messageApiService)
        {
            _messageApiService = messageApiService;
        }
        public Task<ResponceResult> SendText(CommonMessage message)
        {
            var responceResult = _messageApiService.SendMessage(message);
            return responceResult;
        }
    }
}
