﻿using CommonService.Bl.Interfaces;
using CommonService.Dal.Interfaces.Repositories;
using CommonService.Mapper.BeModels;
using CommonService.Models.Be.Accounts;
using CommonService.Models.Be.Subscriptions;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Bl
{
    public class SubscriptionBl : ISubscriptionBl
    {
        private readonly ISubscriptionRepository _subscriptionRepository;
        private readonly IChannelRepository _channelRepository;
        private readonly IEventCategoryRepository _eventCategoryRepository;
        private readonly IAccountRepository _userRepository;

        public SubscriptionBl(ISubscriptionRepository subscriptionRepository,
            IChannelRepository channelRepository,
            IEventCategoryRepository eventCategoryRepository,
            IAccountRepository userRepository)
        {
            _subscriptionRepository = subscriptionRepository;
            _channelRepository = channelRepository;
            _eventCategoryRepository = eventCategoryRepository;
            _userRepository = userRepository;
        }

        public async Task Create(Guid userId, SubscriptionCreate model)
        {
            var user = await _userRepository.GetAsync(userId);

            var subscriptions = _subscriptionRepository.GetByChanelAndEvent(userId, model.ChannelType.ToString(), model.EventCategoryType.ToString());
            if (subscriptions != null || subscriptions.Count() > 0)
            {
                throw new Exception();
            }

            var newSub = new SubscriptionDb()
            {
                Channel = model.ChannelType.ToString(),
                EventCategory = model.EventCategoryType.ToString(),
                Id = Guid.NewGuid(),
                CreateDate = DateTime.Now,
                AccountId = userId,
            };

            await _subscriptionRepository.CreateAsync(newSub);

        }

        public Task Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<Account> GetUserByContact(AccountByContact userByContact)
        {
            var users = await _userRepository.GetAllAsync();

            var user = users.FirstOrDefault(x => x.Activated == true && x.Contacts.Any(a => a.Type == userByContact.Type.ToString() && a.Value == userByContact.Value));
            if (user == null)
                throw new Exception();

            return  AccountMapper.Map(user);
        }
    }
}
