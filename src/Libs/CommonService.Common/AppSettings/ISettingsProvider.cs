﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Common.AppSettings
{
    public interface ISettingsProvider
    {
        string MessageServiceUrl { get; }
        string MessageServiceLogin { get; }
        string MessageServicePass { get; }
        string MessageServiceToken { get; }
        bool UserMessageService { get; }
    }
}
