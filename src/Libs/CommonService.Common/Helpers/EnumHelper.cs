﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Common.Helpers
{
    public static class EnumHelper<T>
    {
        public static T GetValueFromName(string name)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();

            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DisplayAttribute)) as DisplayAttribute;
                if (attribute != null)
                {
                    if (attribute.Name == name)
                    {
                        return (T)field.GetValue(null);
                    }
                }
                else
                {
                    if (field.Name == name)
                        return (T)field.GetValue(null);
                }
            }

            throw new ArgumentOutOfRangeException("name");
        }

        public static IEnumerable<T> GetValues()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }


        public static IList<T> GetValues(Enum value)
        {
            var enumValues = new List<T>();

            foreach (FieldInfo fi in value.GetType().GetFields(BindingFlags.Static | BindingFlags.Public))
            {
                enumValues.Add((T)Enum.Parse(value.GetType(), fi.Name, false));
            }
            return enumValues;
        }

        public static T Parse(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static IList<string> GetNames(Enum value)
        {
            return value.GetType().GetFields(BindingFlags.Static | BindingFlags.Public).Select(fi => fi.Name).ToList();
        }

        public static IList<string> GetDisplayValues(Enum value)
        {
            return GetNames(value).Select(obj => GetDisplayValue(Parse(obj))).ToList();
        }

        public static string GetDisplayValueToLower(T value)
        {
            var result = GetDisplayValue(value);
            return result.ToLower();
        }

        public static string GetDisplayValue(T value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());

            var descriptionAttributes = fieldInfo.GetCustomAttributes(
                typeof(DisplayAttribute), false) as DisplayAttribute[];

            if (descriptionAttributes == null) return string.Empty;
            return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : value.ToString();
        }

        /// <summary>
        /// Получить колекцию элементов перечисления
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<T> GetCollection()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }


        /// <summary>
        /// Получить колекцию элементов перечисления без указанного
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<T> GetCollectionWithoutCurrent(T besides)
        {
            var items = Enum.GetValues(typeof(T)).Cast<T>().ToList();
            items.Remove(besides);
            return items;
        }


        public static IDictionary<T, string> GetDictionary()
        {
            var collection = Enum.GetValues(typeof(T)).Cast<T>();

            var dictionary = new Dictionary<T, string>();

            foreach (var item in collection)
            {
                dictionary.Add(item, GetDisplayValue(item));
            }

            return dictionary;
        }

        public static IDictionary<string, string> GetDictionaryStr()
        {
            var collection = Enum.GetValues(typeof(T)).Cast<T>();

            var dictionary = new Dictionary<string, string>();

            foreach (var item in collection)
            {
                dictionary.Add(item.ToString(), GetDisplayValue(item));
            }

            return dictionary;
        }

        public static IDictionary<T, string> GetDictionaryWithDisplayName()
        {
            var allElements = new Dictionary<T, string>();

            foreach (var right in GetEnumCollection())
            {
                allElements.Add(right, GetDisplayValue(right));
            }

            return allElements;
        }

        public static IEnumerable<T> GetEnumCollection()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }


        //      var display = Enum.TryParse<T>(item, out var enumType) ? enumType : throw new Exception("Ошибка при парсинге SSH типа");
        //
    }
}
