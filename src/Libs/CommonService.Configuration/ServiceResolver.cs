﻿using CommonService.Bl;
using CommonService.Bl.Helper;
using CommonService.Bl.Interfaces;
using CommonService.Common.AppSettings;
using CommonService.Dal;
using CommonService.Dal.Interfaces.Repositories;
using CommonService.Dal.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CommonService.Configuration
{
    public class ServiceResolver
    {
        public static IServiceCollection Services;

        public static IServiceCollection Configure(IServiceCollection services)
        {
            PrepareBl(services);
            PrepareDal(services);

            services.AddTransient<ISettingsProvider, SettingsProvider>();
            services.AddTransient<Context>();

            Services = services;

            return services;
        }

        private static void PrepareBl(IServiceCollection services)
        {
            // BL
            services.AddTransient<IAccountBl, AccountBl>();
            services.AddTransient<ISubscriptionBl, SubscriptionBl>();
            services.AddTransient<IComputerBl, ComputerBl>();
            services.AddTransient<INetworkAdapterBl, NetworkAdapterBl>();
            services.AddTransient<ICameraBl, CameraBl>();
            services.AddTransient<IHttpRequest, HttpRequest>();

            
        }

        private static void PrepareDal(IServiceCollection services)
        {
            // DAL
            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<ISubscriptionRepository, SubscriptionRepository>();
            services.AddTransient<IChannelRepository, ChannelRepository>();
            services.AddTransient<IEventCategoryRepository, EventCategoryRepository>();
            services.AddTransient<ISubscriptionRepository, SubscriptionRepository>();
            services.AddTransient<ICameraRepository, CameraRepository>();
            services.AddTransient<IComputerRepository, ComputerRepository>();
            services.AddTransient<INetworkAdapterRepository, NetworkAdapterRepository>();
        }
    }
}
