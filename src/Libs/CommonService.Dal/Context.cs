﻿using CommonService.Dal.Mappings;
using Microsoft.EntityFrameworkCore;
using System;

namespace CommonService.Dal
{
    public class Context : DbContext
    {
        private readonly string _connectionString;

        //public Context(string connectionString) : base()
        //{           
        //    _connectionString = connectionString;
        //}

        public Context() : base()
        {
            //var connectionString = @"Data Source=185.165.161.149;Initial Catalog=common_db;persist security info=True;User id=db_admin; Password=2Moonw1GJMWP";
            var connectionString = "Host=185.165.161.149;Port=54350;Database=common_db;Username=db_admin;Password=2Moonw5GJMWP";
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(_connectionString);
            }

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AccountContactMap());
            modelBuilder.ApplyConfiguration(new AccountMap());
            modelBuilder.ApplyConfiguration(new CameraMap());
            //modelBuilder.ApplyConfiguration(new ChannelMap());
            modelBuilder.ApplyConfiguration(new ComputerMap());
            //modelBuilder.ApplyConfiguration(new EventCategoryMap());
            modelBuilder.ApplyConfiguration(new NetworkAdapterMap());
            modelBuilder.ApplyConfiguration(new SubscriptionMap());
        }
    }
}
