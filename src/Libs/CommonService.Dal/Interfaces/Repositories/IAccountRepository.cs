﻿using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Dal.Interfaces.Repositories
{
    public interface IAccountRepository : IBaseRepository<AccountDb>
    {
        Task<IEnumerable<AccountDb>> GetAllByEventCategoryAsync(string eventCategory);
        Task<IEnumerable<AccountDb>> GetByLogin(string login);
        Task<IEnumerable<AccountDb>> GetByContact(string contactType, string contactValue);
        Task<AccountDb> GetFullAsync(Guid id);
    }
}
