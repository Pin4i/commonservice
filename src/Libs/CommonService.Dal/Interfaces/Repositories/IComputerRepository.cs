﻿using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Dal.Interfaces.Repositories
{
    public interface IComputerRepository : IBaseRepository<ComputerDb>
    {
        Task<IEnumerable<ComputerDb>> GetByName(string name);
    }
}
