﻿using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Interfaces.Repositories
{
    public interface IEventCategoryRepository : IBaseRepository<EventCategoryDb>
    {
        EventCategoryDb GetByType(string type);
    }
}
