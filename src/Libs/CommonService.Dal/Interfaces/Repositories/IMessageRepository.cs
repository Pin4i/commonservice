﻿using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Interfaces.Repositories
{
    public interface IMessageRepository : IBaseRepository<MessageDb>
    {
    }
}
