﻿using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Dal.Interfaces.Repositories
{
    public interface INetworkAdapterRepository : IBaseRepository<NetworkAdapterDb>
    {
        Task<IEnumerable<NetworkAdapterDb>> GetByIp(string ip);
        Task<NetworkAdapterDb> GetByMac(string mac);
    }
}
