﻿using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Interfaces.Repositories
{
    public interface ISubscriptionRepository : IBaseRepository<SubscriptionDb>
    {
        IEnumerable<SubscriptionDb> GetByChanelAndEvent(Guid userId, string chanelType, string eventType);
    }
}
