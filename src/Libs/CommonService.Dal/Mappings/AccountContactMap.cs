﻿using CommonService.Models.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Mappings
{
    public class AccountContactMap : IEntityTypeConfiguration<AccountContactDb>
    {
        public void Configure(EntityTypeBuilder<AccountContactDb> builder)
        {
            builder = BaseMap.Build(builder);

            builder.Property(x => x.Value).HasColumnName("value");
            builder.Property(x => x.Type).HasColumnName("type");

            builder.HasOne(x => x.Account).WithMany(x => x.Contacts);

            builder.ToTable("accounts_contacts");
        }
    }
}
