﻿using CommonService.Models.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Mappings
{
    public class AccountMap : IEntityTypeConfiguration<AccountDb>
    {
        public void Configure(EntityTypeBuilder<AccountDb> builder)
        {
            builder = BaseMap.Build(builder);

            builder.Property(x => x.Login).HasColumnName("login");
            builder.Property(x => x.Password).HasColumnName("password");
            builder.Property(x => x.Activated).HasColumnName("activated");

            builder.ToTable("accounts");
        }
    }
}
