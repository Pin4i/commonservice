﻿using CommonService.Models.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Mappings
{
    class BaseMap
    {
        public static EntityTypeBuilder<TEntity> Build<TEntity>(EntityTypeBuilder<TEntity> builder) where TEntity : BaseEntityDb
        {
            builder.Property(x => x.Id).HasColumnName("id");
            builder.Property(x => x.CreateDate).HasColumnName("create_date");

            return builder;
        }
    }
}
