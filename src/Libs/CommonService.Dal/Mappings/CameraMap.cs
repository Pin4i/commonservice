﻿using CommonService.Models.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Mappings
{
    public class CameraMap : IEntityTypeConfiguration<CameraDb>
    {
        public void Configure(EntityTypeBuilder<CameraDb> builder)
        {
            builder = BaseMap.Build(builder);

            builder.Property(x => x.Name).HasColumnName("name");
            builder.Property(x => x.Ip).HasColumnName("ip");
            builder.Property(x => x.BaseUrl).HasColumnName("base_url");
            builder.Property(x => x.OperationUrl).HasColumnName("operation_url");
            builder.Property(x => x.Port).HasColumnName("port");
            builder.Property(x => x.Login).HasColumnName("login");
            builder.Property(x => x.Password).HasColumnName("password");
            builder.Property(x => x.External).HasColumnName("external");

            builder.ToTable("cameras");
        }
    }
}
