﻿using CommonService.Models.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Mappings
{
    public class ComputerMap : IEntityTypeConfiguration<ComputerDb>
    {
        public void Configure(EntityTypeBuilder<ComputerDb> builder)
        {
            builder = BaseMap.Build(builder);

            builder.Property(x => x.Name).HasColumnName("name");
            builder.Property(x => x.Description).HasColumnName("description");

            builder.ToTable("computers");
        }
    }
}
