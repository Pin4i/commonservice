﻿using CommonService.Models.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Mappings
{
    public class NetworkAdapterMap : IEntityTypeConfiguration<NetworkAdapterDb>
    {
        public void Configure(EntityTypeBuilder<NetworkAdapterDb> builder)
        {
            builder = BaseMap.Build(builder);

            builder.Property(x => x.Name).HasColumnName("name");
            builder.Property(x => x.Ip).HasColumnName("ip");
            builder.Property(x => x.Mac).HasColumnName("mac");
            builder.Property(x => x.Port).HasColumnName("port");

            builder.HasOne(x => x.Computer).WithMany(x => x.NetworkAdapters).HasForeignKey(x => x.ComputerId);

            builder.ToTable("network_adapters");
        }
    }
}
