﻿using CommonService.Models.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Mappings
{
    public class SubscriptionMap : IEntityTypeConfiguration<SubscriptionDb>
    {
        public void Configure(EntityTypeBuilder<SubscriptionDb> builder)
        {
            builder = BaseMap.Build(builder);

            builder.Property(x => x.Channel).HasColumnName("channel");
            builder.Property(x => x.EventCategory).HasColumnName("event");
            
            builder.HasOne(x => x.Account).WithMany(x => x.Subscriptions).HasForeignKey(x => x.AccountId);

            builder.ToTable("subscriptions");
        }
    }
}
