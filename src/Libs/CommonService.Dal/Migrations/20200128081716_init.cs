﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CommonService.Dal.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "accounts",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    create_date = table.Column<DateTime>(nullable: false),
                    login = table.Column<string>(nullable: true),
                    password = table.Column<string>(nullable: true),
                    activated = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "cameras",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    create_date = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    ip = table.Column<string>(nullable: true),
                    base_url = table.Column<string>(nullable: true),
                    operation_url = table.Column<string>(nullable: true),
                    port = table.Column<int>(nullable: true),
                    login = table.Column<string>(nullable: true),
                    password = table.Column<string>(nullable: true),
                    external = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cameras", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "channels",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    create_date = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_channels", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "computers",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    create_date = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_computers", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "event_categories",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    create_date = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_event_categories", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "accounts_contacts",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    create_date = table.Column<DateTime>(nullable: false),
                    value = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true),
                    AccountId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts_contacts", x => x.id);
                    table.ForeignKey(
                        name: "FK_accounts_contacts_accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "accounts",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "network_adapters",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    create_date = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    mac = table.Column<string>(nullable: true),
                    ip = table.Column<string>(nullable: true),
                    port = table.Column<string>(nullable: true),
                    ComputerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_network_adapters", x => x.id);
                    table.ForeignKey(
                        name: "FK_network_adapters_computers_ComputerId",
                        column: x => x.ComputerId,
                        principalTable: "computers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "subscriptions",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    create_date = table.Column<DateTime>(nullable: false),
                    ChannelId = table.Column<Guid>(nullable: false),
                    EventCategoryId = table.Column<Guid>(nullable: false),
                    AccountId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_subscriptions", x => x.id);
                    table.ForeignKey(
                        name: "FK_subscriptions_accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "accounts",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_subscriptions_channels_ChannelId",
                        column: x => x.ChannelId,
                        principalTable: "channels",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_subscriptions_event_categories_EventCategoryId",
                        column: x => x.EventCategoryId,
                        principalTable: "event_categories",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_accounts_contacts_AccountId",
                table: "accounts_contacts",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_network_adapters_ComputerId",
                table: "network_adapters",
                column: "ComputerId");

            migrationBuilder.CreateIndex(
                name: "IX_subscriptions_AccountId",
                table: "subscriptions",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_subscriptions_ChannelId",
                table: "subscriptions",
                column: "ChannelId");

            migrationBuilder.CreateIndex(
                name: "IX_subscriptions_EventCategoryId",
                table: "subscriptions",
                column: "EventCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "accounts_contacts");

            migrationBuilder.DropTable(
                name: "cameras");

            migrationBuilder.DropTable(
                name: "network_adapters");

            migrationBuilder.DropTable(
                name: "subscriptions");

            migrationBuilder.DropTable(
                name: "computers");

            migrationBuilder.DropTable(
                name: "accounts");

            migrationBuilder.DropTable(
                name: "channels");

            migrationBuilder.DropTable(
                name: "event_categories");
        }
    }
}
