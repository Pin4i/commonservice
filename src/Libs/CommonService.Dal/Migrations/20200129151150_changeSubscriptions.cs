﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CommonService.Dal.Migrations
{
    public partial class changeSubscriptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_subscriptions_channels_ChannelId",
                table: "subscriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_subscriptions_event_categories_EventCategoryId",
                table: "subscriptions");

            migrationBuilder.DropTable(
                name: "channels");

            migrationBuilder.DropTable(
                name: "event_categories");

            migrationBuilder.DropIndex(
                name: "IX_subscriptions_ChannelId",
                table: "subscriptions");

            migrationBuilder.DropIndex(
                name: "IX_subscriptions_EventCategoryId",
                table: "subscriptions");

            migrationBuilder.DropColumn(
                name: "ChannelId",
                table: "subscriptions");

            migrationBuilder.DropColumn(
                name: "EventCategoryId",
                table: "subscriptions");

            migrationBuilder.AddColumn<string>(
                name: "channel",
                table: "subscriptions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "event",
                table: "subscriptions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "channel",
                table: "subscriptions");

            migrationBuilder.DropColumn(
                name: "event",
                table: "subscriptions");

            migrationBuilder.AddColumn<Guid>(
                name: "ChannelId",
                table: "subscriptions",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "EventCategoryId",
                table: "subscriptions",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "channels",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    create_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    type = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_channels", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "event_categories",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    create_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    type = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_event_categories", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_subscriptions_ChannelId",
                table: "subscriptions",
                column: "ChannelId");

            migrationBuilder.CreateIndex(
                name: "IX_subscriptions_EventCategoryId",
                table: "subscriptions",
                column: "EventCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_subscriptions_channels_ChannelId",
                table: "subscriptions",
                column: "ChannelId",
                principalTable: "channels",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_subscriptions_event_categories_EventCategoryId",
                table: "subscriptions",
                column: "EventCategoryId",
                principalTable: "event_categories",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
