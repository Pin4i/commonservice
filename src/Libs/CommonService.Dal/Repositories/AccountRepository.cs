﻿using CommonService.Dal.Interfaces.Repositories;
using CommonService.Models.Db;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Dal.Repositories
{
    public class AccountRepository : BaseRepository<AccountDb>, IAccountRepository
    {
        public AccountRepository(Context context) : base(context)
        {
        }

        public async Task<IEnumerable<AccountDb>> GetAllByEventCategoryAsync(string eventCategory)
        {
            //var query = _dbSet.Include(x => x.Contacts)
            //                  .Include(x => x.Subscriptions).ThenInclude(x=>x.EventCategory)
            //                  .Include(x=>x.Subscriptions).ThenInclude(x=>x.Channel);

            var query = _context.Set<EventCategoryDb>().Where(x => x.Type == eventCategory).Include(x => x.Subscriptions).ThenInclude(x => x.Account).ToList();

            var lst = new List<AccountDb>();

            foreach (var item in query)
            {
                foreach (var sub in item.Subscriptions)
                {
                    lst.Add(sub.Account);
                }
            }

            return lst;
        }

        public override AccountDb Get(Guid id)
        {
            return _dbSet.Include(x => x.Contacts)
                         .FirstOrDefault(x => x.Id == id);
        }



        public async Task<IEnumerable<AccountDb>> GetByLogin(string login)
        {
            var result = _dbSet.Where(x => x.Login == login);
            return result.ToList();
        }

        public async Task<IEnumerable<AccountDb>> GetByContact(string contactType, string contactValue)
        {
            var users = _dbSet.Include(x => x.Contacts).Where(x => x.Contacts.Any(contact => contact.Type == contactType && contact.Value == contactValue));
            return users.ToList();
        }







        public override async Task<AccountDb> GetAsync(Guid id)
        {
            return _dbSet.Include(x => x.Subscriptions).Include(x=>x.Contacts).FirstOrDefault(x => x.Id == id);
        }

        public async Task<AccountDb> GetFullAsync(Guid id)
        {
            return _dbSet.Include(x => x.Subscriptions).ThenInclude(x => x.EventCategory)
                          .Include(x => x.Subscriptions).ThenInclude(x => x.Channel)
                          .Include(x=>x.Contacts).FirstOrDefault(x => x.Id == id);
        }

        public override async Task<IList<AccountDb>> GetAllAsync(bool deleted = false)
        {
            var users = _dbSet.Include(x => x.Contacts);
            return users.ToList();
        }
    }
}
