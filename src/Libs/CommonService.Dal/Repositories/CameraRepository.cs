﻿using CommonService.Dal.Interfaces.Repositories;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Repositories
{
    public class CameraRepository : BaseRepository<CameraDb>, ICameraRepository
    {
        public CameraRepository(Context context) : base(context)
        {
        }
    }
}
