﻿using CommonService.Dal.Interfaces.Repositories;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Dal.Repositories
{
    public class ChannelRepository : BaseRepository<ChannelDb>, IChannelRepository
    {
        public ChannelRepository(Context context) : base(context)
        {
        }

        public ChannelDb GetByType(string type)
        {
            return _dbSet.FirstOrDefault(x => x.Type == type);
        }
    }
}
