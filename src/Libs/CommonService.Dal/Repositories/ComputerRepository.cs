﻿using CommonService.Dal.Interfaces.Repositories;
using CommonService.Models.Db;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Dal.Repositories
{
    public class ComputerRepository : BaseRepository<ComputerDb>, IComputerRepository
    {
        public ComputerRepository(Context context) : base(context)
        {
        }

        public Task<IEnumerable<ComputerDb>> GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public override async Task<ComputerDb> GetAsync(Guid id)
        {
            return await _dbSet.Include(x => x.NetworkAdapters).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
