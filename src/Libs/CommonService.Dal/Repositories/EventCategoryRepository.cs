﻿using CommonService.Dal.Interfaces.Repositories;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Dal.Repositories
{
    public class EventCategoryRepository : BaseRepository<EventCategoryDb>, IEventCategoryRepository
    {
        public EventCategoryRepository(Context context) : base(context)
        {
        }

        public EventCategoryDb GetByType(string type)
        {
            return _dbSet.FirstOrDefault(x => x.Type == type);
        }
    }
}
