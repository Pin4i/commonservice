﻿using CommonService.Dal.Interfaces.Repositories;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Dal.Repositories
{
    public class MessageRepository : BaseRepository<MessageDb>, IMessageRepository
    {
        public MessageRepository(Context context) : base(context)
        {
        }
    }
}
