﻿using CommonService.Dal.Interfaces.Repositories;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Dal.Repositories
{
    public class NetworkAdapterRepository : BaseRepository<NetworkAdapterDb>, INetworkAdapterRepository
    {
        public NetworkAdapterRepository(Context context) : base(context)
        {
        }

        public Task<IEnumerable<NetworkAdapterDb>> GetByIp(string ip)
        {
            throw new NotImplementedException();
        }

        public Task<NetworkAdapterDb> GetByMac(string mac)
        {
            throw new NotImplementedException();
        }
    }
}
