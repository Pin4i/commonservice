﻿using CommonService.Dal.Interfaces.Repositories;
using CommonService.Models.Db;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Dal.Repositories
{
    public class SubscriptionRepository : BaseRepository<SubscriptionDb>, ISubscriptionRepository
    {
        public SubscriptionRepository(Context context) : base(context)
        {
        }

        public IEnumerable<SubscriptionDb> GetByChanelAndEvent(Guid userId, string chanelType, string eventType)
        {
            return _dbSet.Where(x => x.Channel == chanelType && x.EventCategory == eventType && x.AccountId == userId);
        }
    }
}
