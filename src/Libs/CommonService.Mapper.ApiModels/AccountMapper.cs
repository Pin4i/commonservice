﻿using CommonService.Models.Api.In.Account;
using CommonService.Models.Api.Out.Accounts;
using CommonService.Models.Be.Accounts;
using CommonService.Models.Be.Enums;
using CommonService.Models.Be.Subscriptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CommonService.Mapper.ApiModels
{
    public class AccountMapper
    {
        public static AccountCreate Map(AccountCreateModel model)
        {
            return new AccountCreate()
            {
                Login = model.Login,
                Password = model.Password,
                Contacts = model.Subscriptions == null ? null : model.Contacts.Select(x => new AccountContactCreate()
                {
                    Type = (AccountContactTypeEnum)x.Type,
                    Value = x.Value,
                }).ToList(),
                Subscriptions = model.Subscriptions == null ? null : model.Subscriptions.Select(x => new SubscriptionCreate()
                {
                    ChannelType = (ChannelTypeEnum)x.ChannelType,
                    EventCategoryType = (EventCategoryTypeEnum)x.EventCategoryType
                }).ToList(),
            };
        }

        public static IEnumerable<AccountCreate> Map(IEnumerable<AccountCreateModel> models)
        {
            return models.Select(Map).ToList();
        }


        public static AccountByContact Map(AccountByContactModel model)
        {
            return new AccountByContact()
            {
                Type = (AccountContactTypeEnum)model.Type,
                Value = model.Value,

            };
        }

        public static AccountModel Map(Account be)
        {
            return new AccountModel()
            {
                Activated = be.Activated,
                Id = be.Id,
                Login = be.Login,
            };
        }

        public static IEnumerable<AccountModel> Map(IEnumerable<Account> be)
        {
            return be.Select(Map);
        }
    }
}
