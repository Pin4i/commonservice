﻿using CommonService.Models.Api.In.Camera;
using CommonService.Models.Api.Out.Camera;
using CommonService.Models.Be.Cameras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Mapper.ApiModels
{
    public class CameraMap
    {
        public static CameraModel Map(Camera db)
        {
            return new CameraModel()
            {
                BaseUrl = db.BaseUrl,
                CreateDate = db.CreateDate,
                Id = db.Id,
                Ip = db.Ip,
                Login = db.Login,
                Name = db.Name,
                OperationUrl = db.OperationUrl,
                Password = db.Password,
                Port = db.Port,
            };
        }

        public static IEnumerable<CameraModel> Map(IEnumerable<Camera> dbs)
        {
            return dbs.Select(Map);
        }


        public static CameraCreate Map(CameraCreateModel db)
        {
            return new CameraCreate()
            {
                BaseUrl = db.BaseUrl,
                Ip = db.Ip,
                Login = db.Login,
                Name = db.Name,
                OperationUrl = db.OperationUrl,
                Password = db.Password,
                Port = db.Port,
            };
        }

        public static CameraUpdate Map(CameraUpdateModel db)
        {
            return new CameraUpdate()
            {
                Id = db.Id,
                BaseUrl = db.BaseUrl,
                Ip = db.Ip,
                Login = db.Login,
                Name = db.Name,
                OperationUrl = db.OperationUrl,
                Password = db.Password,
                Port = db.Port,
            };
        }
    }
}
