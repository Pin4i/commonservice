﻿using CommonService.Models.Api.In.Computer;
using CommonService.Models.Api.Out.Computer;
using CommonService.Models.Be.Computers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Mapper.ApiModels
{
    public class ComputerMapper
    {
        public static ComputerCreate Map(ComputerCreateModel model)
        {
            return new ComputerCreate()
            {
                Name = model.Name,
                Description = model.Description,
                Adapters = NetworkAdapterMapper.Map(model.Adapters),
            };
        }


        public static ComputerUpdate Map(ComputerUpdateModel model)
        {
            return new ComputerUpdate()
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
            };
        }

        public static ComputerModel Map(Computer be)
        {
            return new ComputerModel()
            {
                Id = be.Id,
                Name = be.Name,
                Description = be.Description,
                Adapters = NetworkAdapterMapper.Map(be.Adapters),
            };
        }

        public static IEnumerable<ComputerModel> Map(IEnumerable<Computer> be)
        {
            return be.Select(Map);
        }
    }
}
