﻿using CommonService.Models.Api.In.Message;
using CommonService.Models.Be.Enums;
using CommonService.Models.Be.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Mapper.ApiModels
{
    public class MessageMapper
    {
        public static Message Map(MessageModel model)
        {
            return new Message()
            {
                Text = model.Text,
                Theme = model.Theme,
                EventCategory = (EventCategoryTypeEnum)model.EventCategory,
                Additional = model.Additional,
                Links = model.Links,
                Tags = model.Tags,
                EventDate = model.EventDate,
            };
        }

        public static IEnumerable<Message> Map(IEnumerable<MessageModel> models)
        {
            return models.Select(Map).ToList();
        }
    }
}
