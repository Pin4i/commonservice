﻿using CommonService.Models.Api.In.NetworkAdapter;
using CommonService.Models.Api.Out.NetworkAdapter;
using CommonService.Models.Be.NetworkAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Mapper.ApiModels
{
    public class NetworkAdapterMapper
    {
        public static NetworkAdapterCreate Map(NetworkAdapterCreateModel model)
        {
            return new NetworkAdapterCreate()
            {
                Ip = model.Ip,
                Mac = model.Mac,
                Name = model.Name,
                Port = model.Port,
                ComputerId = model.ComputerId
            };
        }

        public static IEnumerable<NetworkAdapterCreate> Map(IEnumerable<NetworkAdapterCreateModel> models)
        {
            return models.Select(Map);
        }


        public static NetworkAdapterModel Map(NetworkAdapter model)
        {
            return new NetworkAdapterModel()
            {
                Ip = model.Ip,
                Mac = model.Mac,
                Name = model.Name,
                Port = model.Port,
                id = model.Id
            };
        }

        public static IEnumerable<NetworkAdapterModel> Map(IEnumerable<NetworkAdapter> models)
        {
            return models.Select(Map);
        }
    }
}
