﻿using CommonService.Models.Be.Accounts;
using CommonService.Models.Be.Enums;
using CommonService.Models.Be.Subscriptions;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Mapper.BeModels
{
    public static class AccountMapper
    {
        public static Account Map(AccountDb db)
        {
            var user = new Account()
            {
                Id = db.Id,
                Login = db.Login,
                Activated = db.Activated,
                Subscriptions = db.Subscriptions == null ? null : db.Subscriptions.Select(x => new Subscription()
                {
                    Channel = Enum.Parse<ChannelTypeEnum>(x.Channel),
                    EventCategory = Enum.Parse<EventCategoryTypeEnum>(x.EventCategory),
                }),

                Contacts = db.Contacts == null ? null : db.Contacts.Select(x => new AccountContact()
                {
                    Type = Enum.Parse<ChannelTypeEnum>(x.Type),
                    Value = x.Value,
                })
            };

            return user;
        }

        public static IEnumerable<Account> Map(IEnumerable<AccountDb> dbs)
        {
            return dbs.Select(Map).ToList();
        }
    }
}
