﻿using CommonService.Models.Be.Cameras;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Mapper.BeModels
{
    public class CameraMap
    {
        public static Camera Map(CameraDb db)
        {
            return new Camera()
            {
                BaseUrl = db.BaseUrl,
                CreateDate = db.CreateDate,
                Id = db.Id,
                Ip = db.Ip,
                Login = db.Login,
                Name = db.Name,
                OperationUrl = db.OperationUrl,
                Password = db.Password,
                Port = db.Port,
            };
        }

        public static IEnumerable<Camera> Map(IEnumerable<CameraDb> dbs)
        {
            return dbs.Select(Map);
        }
    }
}
