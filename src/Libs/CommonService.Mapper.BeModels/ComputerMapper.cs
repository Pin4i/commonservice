﻿using CommonService.Models.Be.Computers;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Mapper.BeModels
{
    public class ComputerMapper
    {
        public static Computer Map(ComputerDb be)
        {
            return new Computer()
            {
                Id = be.Id,
                Name = be.Name,
                Description = be.Description,
                Adapters = NetworkAdapterMapper.Map(be.NetworkAdapters),
            };
        }

        public static IEnumerable<Computer> Map(IEnumerable<ComputerDb> be)
        {
            return be.Select(Map);
        }
    }
}
