﻿using CommonService.Models.Be.NetworkAdapters;
using CommonService.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonService.Mapper.BeModels
{
    public class NetworkAdapterMapper
    {

        public static NetworkAdapter Map(NetworkAdapterDb model)
        {
            return new NetworkAdapter()
            {
                Ip = model.Ip,
                Mac = model.Mac,
                Name = model.Name,
                Port = model.Port,
                Id = model.Id
            };
        }

        public static IEnumerable<NetworkAdapter> Map(IEnumerable<NetworkAdapterDb> models)
        {
            return models.Select(Map);
        }
    }
}
