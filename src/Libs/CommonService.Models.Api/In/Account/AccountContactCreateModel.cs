﻿using CommonService.Models.Api.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.In.Account
{
    public class AccountContactCreateModel
    {
        public ChannelTypeEnumModel Type { get; set; }
        public string Value { get; set; }
    }
}
