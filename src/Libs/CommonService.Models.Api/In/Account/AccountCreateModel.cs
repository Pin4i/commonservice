﻿using CommonService.Models.Api.In.Subscription;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.In.Account
{
    public class AccountCreateModel
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public IEnumerable<AccountContactCreateModel> Contacts { get; set; }
        public IEnumerable<SubscriptionModel> Subscriptions { get; set; }
    }
}
