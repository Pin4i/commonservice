﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.In.Camera
{
    public class CameraUpdateModel : CameraCreateModel
    {
        public Guid Id { get; set; }
    }
}
