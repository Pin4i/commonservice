﻿using CommonService.Models.Api.In.NetworkAdapter;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.In.Computer
{
    public class ComputerCreateModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<NetworkAdapterCreateModel> Adapters { get; set; }
    }
}
