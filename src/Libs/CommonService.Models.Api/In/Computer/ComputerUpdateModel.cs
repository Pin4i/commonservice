﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.In.Computer
{
    public class ComputerUpdateModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
