﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.In.Message
{
    public class MessageFilter
    {
        public DateTime BeginEventDate { get; set; }
        public DateTime EndEventDate { get; set; }
    }
}
