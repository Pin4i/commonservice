﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.In.NetworkAdapter
{
    public class NetworkAdapterUpdateModel : NetworkAdapterCreateModel
    {
        public Guid Id { get; set; }

    }
}
