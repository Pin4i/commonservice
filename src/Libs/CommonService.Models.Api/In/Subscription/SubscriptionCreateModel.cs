﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.In.Subscription
{
    public class SubscriptionCreateModel
    {
        public Guid UserId { get; set; }
        public SubscriptionModel Subscription { get; set; }

        //public IEnumerable<SubscriptionModel> Subscriptions { get; set; }
    }
}
