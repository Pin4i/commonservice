﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.In.Subscription
{
    public class SubscriptionListModel
    {
        public IEnumerable<SubscriptionModel> Subscriptions { get; set; }
    }
}
