﻿using CommonService.Models.Api.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.In.Subscription
{
    public class SubscriptionModel
    {
        public ChannelTypeEnumModel ChannelType { get; set; }
        public EventCategoryEnumModel EventCategoryType { get; set; }
    }
}
