﻿using CommonService.Models.Api.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.Out.Accounts
{
    public class AccountContactModel
    {
        public string Value { get; set; }
        public ChannelTypeEnumModel Type { get; set; }
    }
}
