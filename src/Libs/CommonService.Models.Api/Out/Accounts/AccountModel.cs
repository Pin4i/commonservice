﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.Out.Accounts
{
    public class AccountModel
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public bool Activated { get; set; }
        //public IEnumerable<AccountContact> Contacts { get; set; }
        //public IEnumerable<Subscription> Subscriptions { get; set; }
    }
}
