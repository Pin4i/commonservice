﻿using CommonService.Models.Api.Out.NetworkAdapter;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.Out.Computer
{
    public class ComputerModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<NetworkAdapterModel> Adapters { get; set; }
    }
}
