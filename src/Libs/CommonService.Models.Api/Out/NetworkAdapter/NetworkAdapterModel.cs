﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Api.Out.NetworkAdapter
{
    public class NetworkAdapterModel
    {
        public Guid id { get; set; }
        public string Name { get; set; }
        public string Mac { get; set; }
        public string Ip { get; set; }
        public string Port { get; set; }

    }
}
