﻿using CommonService.Models.Be.Enums;
using CommonService.Models.Be.Subscriptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Accounts
{
    public class Account
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public bool Activated { get; set; }
        public IEnumerable<AccountContact> Contacts { get; set; }
        public IEnumerable<Subscription> Subscriptions { get; set; }
    }   
}

