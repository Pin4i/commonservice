﻿using CommonService.Models.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Accounts
{
    public class AccountContact
    {
        public string Value { get; set; }
        public ChannelTypeEnum Type { get; set; }
    }
}
