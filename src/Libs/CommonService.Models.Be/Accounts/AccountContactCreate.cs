﻿using CommonService.Models.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Accounts
{
    public class AccountContactCreate
    {
        public string Value { get; set; }
        public AccountContactTypeEnum Type { get; set; }
    }
}
