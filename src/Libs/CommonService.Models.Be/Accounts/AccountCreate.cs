﻿using CommonService.Models.Be.Channels;
using CommonService.Models.Be.EventCategories;
using CommonService.Models.Be.Subscriptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Accounts
{
    public class AccountCreate
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public IEnumerable<AccountContactCreate> Contacts { get; set; }
        //public IEnumerable<Channel> Channels { get; set; }
        //public IEnumerable<EventCategory> EventCategories { get; set; }
        public IEnumerable<SubscriptionCreate> Subscriptions { get; set; }
    }
}
