﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Cameras
{
    public class CameraCreate
    {
        public string Name { get; set; }
        public string Ip { get; set; }
        public string BaseUrl { get; set; }
        public string OperationUrl { get; set; }
        public int? Port { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
