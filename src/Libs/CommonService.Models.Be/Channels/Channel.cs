﻿using CommonService.Models.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Channels
{
    public class Channel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ChannelTypeEnum Type { get; set; }
    }
}
