﻿using CommonService.Models.Be.NetworkAdapters;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Computers
{
    public class Computer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<NetworkAdapter> Adapters { get; set; }
    }
}
