﻿using CommonService.Models.Be.NetworkAdapters;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Computers
{
    public class ComputerCreate
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<NetworkAdapterCreate> Adapters { get; set; }
    }
}
