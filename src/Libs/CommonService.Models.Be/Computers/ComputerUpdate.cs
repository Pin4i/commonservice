﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Computers
{
    public class ComputerUpdate
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
