﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Enums
{
    public enum ChannelTypeEnum
    {
        Email,
        Telegram,
    }
}
