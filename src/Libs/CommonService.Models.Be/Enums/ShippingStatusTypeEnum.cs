﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Enums
{
    public enum ShippingStatusTypeEnum
    {
        Created,
        InProcessing,
        Sended,
        Error,
    }
}
