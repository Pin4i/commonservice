﻿using CommonService.Models.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.EventCategories
{
    public class EventCategory
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public EventCategoryTypeEnum Type { get; set; }
    }
}
