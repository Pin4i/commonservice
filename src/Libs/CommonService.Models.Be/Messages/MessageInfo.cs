﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Messages
{
    public class MessageInfo
    {
       public IDictionary<string, string> Links { get; set; } 
       public IEnumerable<string> Tags { get; set; } 
       public string SendTo { get; set; }
        public string ChanelType { get; set; }
        public Guid AccountId { get; set; }
    }
}
