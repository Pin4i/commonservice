﻿using CommonService.Models.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Messages
{
    public class ShippingInformation
    {
        public ChannelTypeEnum Channel { get; set; }
        public EventCategoryTypeEnum EventCategory { get; set; }
        public ShippingStatusTypeEnum Status { get; set; }
        public string Error { get; set; }
        public Guid UserId { get; set; }
        public string ContactInfo { get; set; }
        public DateTime SendedDateTime { get; set; }
    }
}
