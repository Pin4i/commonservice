﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Messages
{
    public enum ShippingStatus
    {
        Created,
        InProcessing,
        Sended,
        Error,
    }
}
