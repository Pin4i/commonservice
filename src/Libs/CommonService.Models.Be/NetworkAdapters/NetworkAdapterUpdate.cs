﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.NetworkAdapters
{
    public class NetworkAdapterUpdate : NetworkAdapterCreate
    {
        public Guid Id { get; set; }
    }
}
