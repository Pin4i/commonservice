﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be
{
    public class SendResult
    {
        public bool Error { get; set; }
        public string ErrorText { get; set; }
    }
}
