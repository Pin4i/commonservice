﻿using CommonService.Models.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Subscriptions
{
    public class Subscription
    {
        public ChannelTypeEnum Channel { get; set; }
        public EventCategoryTypeEnum EventCategory { get; set; }
    }
}
