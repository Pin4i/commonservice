﻿using CommonService.Models.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Be.Subscriptions
{
    public class SubscriptionCreate
    {
        public Guid AccountId { get; set; }
        public ChannelTypeEnum ChannelType { get; set; }
        public EventCategoryTypeEnum EventCategoryType { get; set; }
    }
}
