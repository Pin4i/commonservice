﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Db
{
    public class AccountContactDb : BaseEntityDb
    {
        public string Value { get; set; }
        public string Type { get; set; }

        public Guid AccountId { get; set; }
        public AccountDb Account { get; set; }
    }
}
