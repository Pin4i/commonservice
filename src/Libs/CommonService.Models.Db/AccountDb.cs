﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Db
{
    public class AccountDb : BaseEntityDb
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public bool Activated { get; set; }

        public IEnumerable<AccountContactDb> Contacts { get; set; }

        public IEnumerable<SubscriptionDb> Subscriptions { get; set; }
    }
}
