﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Db
{
    public class BaseEntityDb
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
