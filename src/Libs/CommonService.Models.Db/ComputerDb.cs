﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Db
{
    public class ComputerDb  :BaseEntityDb
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<NetworkAdapterDb> NetworkAdapters { get; set; }
    }
}
