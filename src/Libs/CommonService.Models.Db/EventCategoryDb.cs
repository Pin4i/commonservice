﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Db
{
    public class EventCategoryDb : BaseEntityDb
    {
        public string Name { get; set; }
        public string Type { get; set; }

        public IEnumerable<SubscriptionDb> Subscriptions { get; set; }
    }
}
