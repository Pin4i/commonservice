﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Db
{
    public class MessageDb : BaseEntityDb
    {
        public DateTime EventDate { get; set; }
        public string Theme { get; set; }
        public string Text { get; set; }
        public string Additional { get; set; }
        public string Tags { get; set; }
        public string Links { get; set; }
        public string EventCategoryType { get; set; }
        public IEnumerable<ShippingInformationDb> ShippingInformations { get; set; }
    }
}
