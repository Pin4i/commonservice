﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Db
{
    public class NetworkAdapterDb : BaseEntityDb
    {
        public string Name { get; set; }
        public string Mac { get; set; }
        public string Ip { get; set; }
        public string Port { get; set; }

        public Guid ComputerId { get; set; }
        public ComputerDb Computer { get; set; }
    }
}
