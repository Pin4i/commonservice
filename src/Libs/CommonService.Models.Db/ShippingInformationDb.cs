﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Db
{
    public class ShippingInformationDb : BaseEntityDb
    {
        public string Channel { get; set; }
        public string EventCategory { get; set; }
        public Guid UserId { get; set; }
        public string SendTo { get; set; }
        public string Status { get; set; }
        public string Error { get; set; }

        public MessageDb Message { get; set; }
    }
}
