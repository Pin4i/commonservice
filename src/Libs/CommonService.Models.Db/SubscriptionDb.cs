﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonService.Models.Db
{
    public class SubscriptionDb : BaseEntityDb
    {
        public string Channel { get; set; }
        public string EventCategory { get; set; }


        public Guid AccountId { get; set; }
        public AccountDb Account { get; set; }
    }
}
