﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CameraRequestService.ApiModelMapper;
using CameraRequestService.ApiModels;
using CameraRequestService.Bl.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CommonService.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CameraController : ControllerBase
    {
        private readonly ICameraBl _cameraBl;

        public CameraController(ICameraBl cameraBl)
        {
            _cameraBl = cameraBl;
        }

        [HttpPost("create")]
        public async Task<CameraModel> Create(CameraCreateModel model)
        {
            var camera = await _cameraBl.CreateAsync(CameraModelMap.Map(model));
            return CameraModelMap.Map(camera);
        }

        [HttpPost("update")]
        public async Task<CameraModel> Update(CameraUpdateModel model)
        {
            var camera = await _cameraBl.UpdateAsync(CameraModelMap.Map(model));
            return CameraModelMap.Map(camera);
        }

        [HttpPost("get/{id}")]
        public async Task<CameraModel> Get(Guid id)
        {
            var camera = await _cameraBl.GetAsync(id);
            return CameraModelMap.Map(camera);
        }

        [HttpPost("getall")]
        public async Task<IEnumerable<CameraModel>> GetAll()
        {
            var cameras = await _cameraBl.GetAllAsync();
            return CameraModelMap.Map(cameras);
        }

        [HttpPost("getphoto/{cameraId}")]
        public async Task<byte[]> GetPhoto(Guid cameraId)
        {
            //return await _cameraBl.GetPhotoAsync(cameraId);
            return null;
        }
    }
}