﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonService.Bl.Interfaces;
using CommonService.Mapper.ApiModels;
using CommonService.Models.Api.In.Account;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace CommonService.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IAccountBl _userBl;
        private readonly ISubscriptionBl _subscriptionBl;

        public UserController(IAccountBl userBl, ISubscriptionBl subscriptionBl)
        {
            if (userBl == null) throw new ArgumentNullException("userBl");
            _userBl = userBl;
            _subscriptionBl = subscriptionBl;
        }

        [HttpPost("create")]
        public async Task CreateUser(AccountCreateModel model)
        {
            await _userBl.CreateAsync(AccountMapper.Map(model));
        }

        [HttpPost("createsubscription")]
        public async Task CreateSubscription(SubscriptionCreateModel model)
        {
            await _subscriptionBl.Create(model.UserId, new SubscriptionCreate()
            {
                ChannelType = (ChannelTypeEnum)model.Subscription.ChannelType,
                EventCategoryType = (EventCategoryTypeEnum)model.Subscription.EventCategoryType,
            });
        }

        [HttpPost("deletesubscription/{id}")]
        public async Task DeleteSubscription(Guid id)
        {
            await _subscriptionBl.Delete(id);
        }

        [HttpPost("getbycontact")]
        public async Task<dynamic> GetByContact(AccountByContactModel model)
        {
            var user = await _subscriptionBl.GetUserByContact(AccountMapper.Map(model));
            return user;
        }

        [HttpGet("activate/{id}")]
        public async Task<dynamic> Activate(Guid id)
        {
            var user = await _userBl.Activate(id);
            return user;
        }

        [HttpGet("diactivate/{id}")]
        public async Task<dynamic> Diactivate(Guid id)
        {
            var user = await _userBl.Diactivate(id);
            return user;
        }
    }
}