﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserInfoService.Configuration;

namespace CommonService.App.Infrastructure
{
    public class DependencyResolver
    {
        public static IServiceCollection Services;

        public static void Configure(IServiceCollection services)
        {
            UserInfoServiceResolver.Configure(services);

            Services = services;
        }
    }
}
