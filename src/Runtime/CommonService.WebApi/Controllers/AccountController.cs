﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonService.Bl.Interfaces;
using CommonService.Mapper.ApiModels;
using CommonService.Models.Api.In.Account;
using CommonService.Models.Api.In.Subscription;
using CommonService.Models.Api.Out.Accounts;
using CommonService.Models.Be.Enums;
using CommonService.Models.Be.Subscriptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace CommonService.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountBl _userBl;
        private readonly ISubscriptionBl _subscriptionBl;

        public AccountController(IAccountBl userBl, ISubscriptionBl subscriptionBl)
        {
            if (userBl == null) throw new ArgumentNullException("userBl");
            if (subscriptionBl == null) throw new ArgumentNullException("subscriptionBl");

            _userBl = userBl;
            _subscriptionBl = subscriptionBl;
        }

        /// <summary>
        /// Создание учетной записи
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task CreateUser(AccountCreateModel model)
        {
            await _userBl.CreateAsync(AccountMapper.Map(model));
        }

        /// <summary>
        /// Создание подписки
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("createsubscription")]
        public async Task CreateSubscription(SubscriptionCreateModel model)
        {
            await _subscriptionBl.Create(model.UserId, new SubscriptionCreate()
            {
                ChannelType = (ChannelTypeEnum)model.Subscription.ChannelType,
                EventCategoryType = (EventCategoryTypeEnum)model.Subscription.EventCategoryType,
            });
        }

        /// <summary>
        /// Удаление подписки
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("deletesubscription/{id}")]
        public async Task DeleteSubscription(Guid id)
        {
            await _subscriptionBl.Delete(id);
        }


        [HttpPost("getbycontact")]
        public async Task<dynamic> GetByContact(AccountByContactModel model)
        {
            var user = await _subscriptionBl.GetUserByContact(AccountMapper.Map(model));
            return user;
        }

        /// <summary>
        /// Активировать учетную запись
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("activate/{id}")]
        public async Task<dynamic> Activate(Guid id)
        {
            var user = await _userBl.Activate(id);
            return user;
        }

        /// <summary>
        /// Деактивировать учетную запись
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("diactivate/{id}")]
        public async Task<dynamic> Diactivate(Guid id)
        {
            var user = await _userBl.Diactivate(id);
            return user;
        }

        /// <summary>
        /// Получить все учетки
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getall")]
        public async Task<IEnumerable<AccountModel>> GetAll()
        {
            var users = await _userBl.GetAll();
            return AccountMapper.Map(users);
        }

        /// <summary>
        /// Получить учетку по ИД
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get/{id}")]
        public async Task<AccountModel> Get(Guid id)
        {
            var user = await _userBl.GetById(id);
            return AccountMapper.Map(user);
        }
    }
}