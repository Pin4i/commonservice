﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonService.Bl.Interfaces;
using CommonService.Mapper.ApiModels;
using CommonService.Models.Api.In.Camera;
using CommonService.Models.Api.Out.Camera;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CommonService.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CameraController : ControllerBase
    {
        private readonly ICameraBl _cameraBl;

        public CameraController(ICameraBl cameraBl)
        {
            _cameraBl = cameraBl;
        }

        [HttpPost("create")]
        public async Task<CameraModel> Create(CameraCreateModel model)
        {
            var camera = await _cameraBl.CreateAsync(CameraMap.Map(model));
            return CameraMap.Map(camera);
        }

        [HttpPost("update")]
        public async Task<CameraModel> Update(CameraUpdateModel model)
        {
            var camera = await _cameraBl.UpdateAsync(CameraMap.Map(model));
            return CameraMap.Map(camera);
        }

        [HttpPost("get/{id}")]
        public async Task<CameraModel> Get(Guid id)
        {
            var camera = await _cameraBl.GetAsync(id);
            return CameraMap.Map(camera);
        }

        [HttpPost("getall")]
        public async Task<IEnumerable<CameraModel>> GetAll()
        {
            var cameras = await _cameraBl.GetAllAsync();
            return CameraMap.Map(cameras);
        }

        [HttpPost("getphoto/{cameraId}")]
        public async Task<byte[]> GetPhoto(Guid cameraId)
        {
            //return await _cameraBl.GetPhotoAsync(cameraId);
            return null;
        }
    }
}