﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonService.Bl.Interfaces;
using CommonService.Mapper.ApiModels;
using CommonService.Models.Api.In.Computer;
using CommonService.Models.Api.Out.Computer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CommonService.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WolController : ControllerBase
    {
        private readonly IComputerBl _computerBl;
        public WolController(IComputerBl computerBl)
        {
            _computerBl = computerBl;
        }

        [HttpPost("create")]
        public async Task Create(ComputerCreateModel model)
        {
            var info = await _computerBl.Create(ComputerMapper.Map(model));
        }

        [HttpPost("update")]
        public async Task Update(ComputerUpdateModel model)
        {
            var info = await _computerBl.Update(ComputerMapper.Map(model));
        }

        [HttpGet("delete/{id}")]
        public async Task Delete(Guid id)
        {
            await _computerBl.Delete(id);
        }

        [HttpGet("get/{id}")]
        public async Task<ComputerModel> Get(Guid id)
        {
            var info = await _computerBl.Get(id);
            return ComputerMapper.Map(info);
        }

        [HttpGet("getall")]
        public async Task<IEnumerable<ComputerModel>> GetAll()
        {
            return ComputerMapper.Map(await _computerBl.GetAll());
        }
    }
}