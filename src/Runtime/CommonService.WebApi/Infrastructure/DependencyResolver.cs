﻿using CommonService.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommonService.WebApi.Infrastructure
{
    public class DependencyResolver
    {
        public static IServiceCollection Configure(IServiceCollection services)
        {
            return ServiceResolver.Configure(services);
        }
    }
}
