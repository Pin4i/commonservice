﻿using CameraRequestService.Bl;
using CameraRequestService.Bl.Helpers;
using CameraRequestService.Bl.Interfaces;
using CameraRequestService.Bl.Interfaces.Helpers;
using CameraRequestService.Dal;
using CameraRequestService.Dal.Interfaces.Repositories;
using CameraRequestService.Dal.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace WakeOnLanService.Configuration
{
    public class WakeOnLanResolver
    {
        public static IServiceCollection Services;

        public static void Configure(IServiceCollection services)
        {
            PrepareBl(services);
            PrepareServices(services);
            PrepareDal(services);
            
            Services = services;
        }

        private static void PrepareBl(IServiceCollection services)
        {

        }

        private static void PrepareDal(IServiceCollection services)
        {
           

        }

        private static void PrepareServices(IServiceCollection services)
        {

        }
    }
}
