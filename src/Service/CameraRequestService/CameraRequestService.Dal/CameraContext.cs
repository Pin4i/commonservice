﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using CameraRequestService.Dal.Mappings;

namespace CameraRequestService.Dal
{
    public class CameraContext : DbContext
    {
        private readonly string _connectionString;

        //public Context(string connectionString) : base()
        //{           
        //    _connectionString = connectionString;
        //}

        public CameraContext() : base()
        {
            var connectionString = @"Data Source=Mega-db;Initial Catalog=camera_db;persist security info=True;User id=sa; Password=04061608";
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer(_connectionString);
            }

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CameraMap());
        }

        //public ModelBuilder RelationMapsConfigure(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.ApplyConfiguration(new UserEventCategoryMap());
        //    modelBuilder.ApplyConfiguration(new UserChannelMap());

        //    return modelBuilder;
        //}
    }  
}
