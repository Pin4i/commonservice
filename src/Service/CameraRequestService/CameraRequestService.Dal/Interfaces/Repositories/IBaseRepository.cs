﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CameraRequestService.DbModels;

namespace CameraRequestService.Dal.Interfaces.Repositories
{
   
        public interface IBaseRepository<TEntity> where TEntity : BaseEntityDb
        {
            // ASYNC

            Task<TEntity> GetAsync(Guid id);

            Task<TEntity> CreateAsync(TEntity entity, bool saveChanges = true);

            Task<TEntity> FindEntityAsync(params object[] keyValues);

            Task<IEnumerable<TEntity>> GetAllAsync(bool deleted = false);

            Task<TEntity> UpdateAsync(TEntity entity, bool saveChanges = true);


            // NOT ASYNC

            TEntity Create(TEntity entity);

            IEnumerable<TEntity> GetWithInclude(params Expression<Func<TEntity, object>>[] includeProperties);

            IEnumerable<TEntity> GetWithInclude(Func<TEntity, bool> predicate,
                params Expression<Func<TEntity, object>>[] includeProperties);

            IQueryable<TEntity> Include(params Expression<Func<TEntity, object>>[] includeProperties);


            void DeleteLater(Guid id);

            bool DeleteNow(TEntity entity, bool saveChanges = true);



            TEntity Get(Guid id);

            IQueryable<TEntity> GetAll(bool deleted = false);




            IEnumerable<TEntity> GetAllShort(bool showDeleted = false);

            TEntity Update(TEntity entity);

            Task SaveChangesAsync();

            void SaveChanges();
        }
    
}
