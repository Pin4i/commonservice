﻿using CameraRequestService.Dal.Interfaces.Repositories;
using CameraRequestService.DbModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace CameraRequestService.Dal.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntityDb
    {
        protected CameraContext _context;
        protected DbSet<TEntity> _dbSet;

        public BaseRepository(CameraContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        // ASYNC

        public virtual async Task<TEntity> GetAsync(Guid id)
        {
            var note = await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
            return note;
        }

        public virtual async Task<TEntity> CreateAsync(TEntity entity, bool saveChanges = true)
        {
            entity.CreateDate = DateTime.Now;

            entity.Id = Guid.NewGuid();

            var result = _context.Set<TEntity>().Add(entity);

            if (saveChanges)
                await SaveChangesAsync();

            return entity;
        }

        public virtual async Task<TEntity> UpdateAsync(TEntity entity, bool saveChanges = true)
        {
            _context.Entry(entity).State = EntityState.Modified;

            if (saveChanges)
                await SaveChangesAsync();

            return entity;
        }

        public async Task<TEntity> FindEntityAsync(params object[] keyValues)
        {
            throw new NotImplementedException();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync(bool deleted = false)
        {
            throw new NotImplementedException();
        }






        // NOT ASYNC

        public TEntity Create(TEntity entity)
        {
            entity.CreateDate = DateTime.Now;

            entity.Id = Guid.NewGuid();

            var result = _context.Set<TEntity>().Add(entity);

            SaveChanges();

            return entity;
        }

        public IEnumerable<TEntity> GetWithInclude(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return Include(includeProperties).ToList();
        }

        public IEnumerable<TEntity> GetWithInclude(Func<TEntity, bool> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var query = Include(includeProperties);
            return query.Where(predicate).ToList();
        }

        public IQueryable<TEntity> Include(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _dbSet.AsNoTracking();
            return includeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        //public TEntity AddOrUpdate(TEntity entity)
        //{
        //    _db
        //}


        public void DeleteLater(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool DeleteNow(TEntity entity, bool saveChanges = true)
        {
            throw new NotImplementedException();
        }



        public virtual TEntity Get(Guid id)
        {
            var entity = _context.Set<TEntity>().FirstOrDefault(x => x.Id == id);
            return entity;
        }

        public virtual IQueryable<TEntity> GetAll(bool deleted = false)
        {
            var query = _context.Set<TEntity>();
            return query;
            throw new NotImplementedException();
        }



        public IEnumerable<TEntity> GetAllShort(bool showDeleted = false)
        {
            throw new NotImplementedException();
        }

        public TEntity Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void SaveChanges()
        {
            try
            {
                _context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
