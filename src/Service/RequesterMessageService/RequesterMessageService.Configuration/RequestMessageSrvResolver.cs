﻿using Microsoft.Extensions.DependencyInjection;
using RequesterMessageService.Lib;
using RequesterMessageService.Lib.Helpers;
using RequesterMessageService.Lib.Interfaces;
using RequesterMessageService.Lib.Interfaces.Helpers;
using System;

namespace RequesterMessageService.Configuration
{
    public class RequestMessageSrvResolver
    {
        public static IServiceCollection Services;

        public static void Configure(IServiceCollection services)
        {
            PrepareBl(services);
            PreparHelper(services);

            Services = services;
        }

        private static void PrepareBl(IServiceCollection services)
        {
            services.AddTransient<IMessageSender, MessageSender>();
        }

        

        private static void PreparHelper(IServiceCollection services)
        {
            services.AddTransient<IHttpRequestHelper, HttpRequestHelper>();

        }
    }
}
