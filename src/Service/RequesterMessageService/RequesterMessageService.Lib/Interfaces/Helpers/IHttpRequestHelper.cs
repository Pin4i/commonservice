﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RequesterMessageService.Lib.Interfaces.Helpers
{
    public interface IHttpRequestHelper
    {
        Task<string> Post(string url, string json);
    }
}
