﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RequesterMessageService.Lib.Models
{
    public class MessageModel
    {
        public string Theme { get; set; }
        public string Text { get; set; }
        public string Additional { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public IDictionary<string, string> Links { get; set; }
    }
}
