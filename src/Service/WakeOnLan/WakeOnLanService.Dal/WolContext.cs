﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WakeOnLanService.Dal.Mappings;

namespace WakeOnLanService.Dal
{
    public class WolContext : DbContext
    {
        private readonly string _connectionString;

        //public Context(string connectionString) : base()
        //{           
        //    _connectionString = connectionString;
        //}

        public WolContext() : base()
        {
            var connectionString = @"Data Source=Mega-db;Initial Catalog=PBook;persist security info=True;User id=sa; Password=04061608";
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer(_connectionString);
            }

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ComputerMap());
            modelBuilder.ApplyConfiguration(new NetworkAdapterMap());
        }

        //public ModelBuilder RelationMapsConfigure(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.ApplyConfiguration(new UserEventCategoryMap());
        //    modelBuilder.ApplyConfiguration(new UserChannelMap());

        //    return modelBuilder;
        //}
    }  
}
